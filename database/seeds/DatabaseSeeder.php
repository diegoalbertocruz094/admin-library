<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    public function run()
    {
        DB::table('student')->insert([
            'name' => Str::random(10)
        ]);
        DB::table('book')->insert([
            'name' => Str::random(10)
        ]);
    }
}
