<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/rent/list', 'LibraryController@index');
Route::get('/rent/add', 'LibraryController@create');
Route::post('/rent/store', 'LibraryController@store');
Route::get('/rent/remove/{id}', 'LibraryController@destroy');
